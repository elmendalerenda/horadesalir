# Dependencies

* npm
* npm install
* npm install -g gulp
* gulp build


Everytime there is a change in css (`src_home/src/assets/css/custom.css`) a new bundle must be created. Follow these steps:

```bash
cd src_home
gulp extreme_build
cp ../template/assets/css/bundle.min.css ../vendor/assets/stylesheets/saastrend/bundle.min.css
```

The initial template is v10.html, although now it is heavily changed
