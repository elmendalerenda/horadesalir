class CreateAccounts < ActiveRecord::Migration[5.2]
  def change
    create_table :accounts do |t|
      t.string :remote_id
      t.string :auth_token
      t.string :calendar_id
      t.string :calendar_name

      t.timestamps
    end
  end
end
