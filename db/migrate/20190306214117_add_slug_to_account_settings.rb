class AddSlugToAccountSettings < ActiveRecord::Migration[5.2]
  def change
    add_column :account_settings, :slug, :string, index: true
    add_index :account_settings, :slug
  end
end
