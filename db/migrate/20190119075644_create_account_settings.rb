class CreateAccountSettings < ActiveRecord::Migration[5.2]
  def change
    create_table :account_settings do |t|
      t.string :name
      t.belongs_to :account, index: true

      t.timestamps
    end
  end
end
