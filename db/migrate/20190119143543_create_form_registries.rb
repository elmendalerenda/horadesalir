class CreateFormRegistries < ActiveRecord::Migration[5.2]
  def change
    create_table :form_registries do |t|
      t.string :form_id
      t.belongs_to :account

      t.timestamps
    end
    add_index :form_registries, :form_id
  end
end
