class AddTimezoneToAccountsettings < ActiveRecord::Migration[5.2]
  def change
    add_column :account_settings, :timezone, :string
  end
end
