class CreateOccupancy < ActiveRecord::Migration[5.2]
  def change
    create_table :occupancies do |t|
      t.datetime :start_time
      t.datetime :end_time
      t.integer :guests
      t.belongs_to :account, index: true

      t.timestamps
    end
  end
end
