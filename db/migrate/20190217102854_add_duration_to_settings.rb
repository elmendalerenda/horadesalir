class AddDurationToSettings < ActiveRecord::Migration[5.2]
  def change
    add_column :account_settings, :duration, :integer, default: 60
  end
end
