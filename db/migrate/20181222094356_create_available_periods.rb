class CreateAvailablePeriods < ActiveRecord::Migration[5.2]
  def change
    create_table :available_periods do |t|
      t.integer :day_of_week
      t.string :start_time
      t.string :end_time
      t.belongs_to :account, index: true

      t.timestamps
    end
  end
end
