class AddMaxGuestsToSettings < ActiveRecord::Migration[5.2]
  def change
    add_column :account_settings, :max_guests, :integer, default: 1
  end
end
