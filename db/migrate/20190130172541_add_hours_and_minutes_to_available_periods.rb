class AddHoursAndMinutesToAvailablePeriods < ActiveRecord::Migration[5.2]
  def change
    add_column :available_periods, :start_hour, :integer
    add_column :available_periods, :start_minute, :integer
    add_column :available_periods, :end_hour, :integer
    add_column :available_periods, :end_minute, :integer
  end
end
