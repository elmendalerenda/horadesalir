require 'test_helper'

class AvailablePeriodTest < ActiveSupport::TestCase
  test 'validates end > start' do
    account = accounts(:one)
    invalid_hour = AvailablePeriod.new(start_hour: 12, end_hour:12, start_minute:0, end_minute:0, account: account)
    invalid_minutes = AvailablePeriod.new(start_hour: 12, end_hour:12, start_minute:1, end_minute:0, account: account)
    equal = AvailablePeriod.new(start_hour: 12, end_hour:12, start_minute:0, end_minute:0, account: account)

    refute(invalid_hour.valid?)
    assert_equal(:within, invalid_hour.errors.details[:end_hour][0][:error])
    refute(invalid_minutes.valid?)
    assert_equal(:within, invalid_minutes.errors.details[:end_hour][0][:error])
    refute(equal.valid?)
    assert_equal(:within, equal.errors.details[:end_hour][0][:error])
  end
end
