require 'test_helper'

class AccountTest < ActiveSupport::TestCase
  test 'raise RecordNotFound if slug is not found' do
    assert_raises(ActiveRecord::RecordNotFound) { Account.find_by_slug('INVALID_SLUG') }
  end
end
