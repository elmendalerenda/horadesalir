require 'test_helper'
require 'webmock/minitest'

class EventsTest < ActiveSupport::TestCase
  setup do
    account = Kloudless::Account.new('id', 'auth_token')
    calendar = Kloudless::Calendar.new('calendar_id', 'calendar_name')
    time_start = Time.utc(2001, 5, 4, 12, 0, 0)
    time_end = Time.utc(2001, 5, 4, 13, 0, 0)
    person_name = 'Laura'
    person_email = 'laura@example.com'
    event_name = 'Event name'
    description = 'hangout for a coffee'
    @event = Kloudless::Event.new(event_name, account, calendar, time_start, time_end, person_name, person_email, description)
  end

  test '#create creates an event in a calendar' do
    expected_request_params = {
      name: @event.name,
      start: @event.time_start.iso8601,
      end: @event.time_end.iso8601,
      description: @event.description,
      organizer: { name: @event.person_name, email: @event.person_email }
    }.to_json
    expected_url = "https://api.kloudless.com/v1/accounts/#{@event.account.id}/cal/calendars/#{@event.calendar.id}/events"
    expected_headers = { 'Content-Type' => 'application/json', 'Authorization' => "Bearer #{@event.account.auth_token}" }

    stub_request(:post, expected_url)
      .with(
        body: expected_request_params,
        headers: expected_headers
      )
      .to_return(status: 200, body: {}.to_json, headers: {})

    Kloudless::Events.create(@event)

    assert_requested :post, expected_url,
                     headers: expected_headers,
                     body: expected_request_params,
                     times: 1
  end

  test '#create raises an error when timeout' do
    stub_request(:post, "https://api.kloudless.com/v1/accounts/#{@event.account.id}/cal/calendars/#{@event.calendar.id}/events").to_timeout

    assert_raises(Kloudless::NetException) do
      Kloudless::Events.create(@event)
    end
  end

  test 'raises error when the response contains an error' do
    stub_request(:post,  "https://api.kloudless.com/v1/accounts/#{@event.account.id}/cal/calendars/#{@event.calendar.id}/events")
      .to_return(status: 200, body: { error_code: '404' }.to_json)

    assert_raises(Kloudless::Error) do
      Kloudless::Events.create(@event)
    end
  end
end
