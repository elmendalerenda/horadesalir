require 'test_helper'
require 'webmock/minitest'
require 'kloudless'

class CalendarsTest < ActiveSupport::TestCase
  test 'returns the calendars for an account' do
    response_body = {
      objects: [
        {
          "name": 'kloudless@gmail.com',
          "id": 'fa2xvdWRsZXNzLnRlc3QudGltb3RoeUBnbWFpbC5jb20=',
          "timezone": 'America/Los_Angeles'
        },
        {
          "name": 'Contacts',
          "id": 'fI2NvbnRhY3RzQGdyb3VwLnYuY2FsZW5kYXIuZ29vZ2xlLmNvbQ==',
          "timezone": 'America/Los_Angeles'
        }
      ]
    }.to_json
    account = Kloudless::Account.new('account_id', '[TOKEN]')

    stub_request(:get, "https://api.kloudless.com/v1/accounts/#{account.id}/cal/calendars")
      .with(
        headers: { 'Content-Type' => 'application/json', 'Authorization' => "Bearer #{account.auth_token}" }
      )
      .to_return(status: 200, body: response_body)

    calendars = Kloudless::Calendars.find_all(account)

    assert_equal('kloudless@gmail.com', calendars.first.name)
    assert_equal('fI2NvbnRhY3RzQGdyb3VwLnYuY2FsZW5kYXIuZ29vZ2xlLmNvbQ==', calendars[1].id)
    assert_equal('America/Los_Angeles', calendars.first.timezone)
  end

  test 'raises error when the connection fails' do
    account = Kloudless::Account.new('account_id', '[TOKEN]')
    stub_request(:get, "https://api.kloudless.com/v1/accounts/#{account.id}/cal/calendars").to_timeout

    assert_raises(Kloudless::NetException) { Kloudless::Calendars.find_all(account) }
  end

  test 'raises error when the response contains an error' do
    account = Kloudless::Account.new('account_id', '[TOKEN]')
    stub_request(:get, "https://api.kloudless.com/v1/accounts/#{account.id}/cal/calendars")
      .to_return(status: 200, body: { error_code: '404' }.to_json)

    assert_raises(Kloudless::Error) { Kloudless::Calendars.find_all(account) }
  end
end
