require 'test_helper'

class MetricsTest < ActiveSupport::TestCase
  test 'counts the number of times a query to the calendar has been made' do
    output = StringIO.new
    Metrics.call(file_fixture('metrics_slots.log'), output, '2019-03-09')

    expected = "I, [2019-03-09T10:39:59.855572 #28529]  INFO -- : [QUERY SLOTS] account: 1, name: Legal Rock, params: {\"controller\"=>\"appointments\", \"action\"=>\"show\", \"slug\"=>\"legal-rock\"}
I, [2019-03-09T10:39:59.855572 #28529]  INFO -- : [QUERY SLOTS] account: 2, name: Legal Pop, params: {\"controller\"=>\"appointments\", \"action\"=>\"show\", \"slug\"=>\"legal-pop\"}
appointment-queries: 2
events-created: 0
new-users: 0
total-users: 0
new-accounts: 0
total-accounts: 0"

    assert_equal(expected, output.string)
  end

  test 'counts the number of events that have been created' do
    output = StringIO.new
    Metrics.call(file_fixture('metrics_events.log'), output, '2019-03-09')

    expected = "I, [2019-03-09T10:52:44.936482 #28529]  INFO -- : [CREATE EVENT] account_id: 1, name: Legal Rock, with <AppointmentForm name:name, date:2019-03-12 15:00:00 +01:00, email:test@fastmail.com, date_from:>
I, [2019-03-09T10:52:44.936482 #28529]  INFO -- : [CREATE EVENT] account_id: 2, name: Legal Pop, with <AppointmentForm name:name, date:2019-03-12 15:00:00 +01:00, email:test@fastmail.com, date_from:>
I, [2019-03-09T10:52:44.936482 #28529]  INFO -- : [CREATE EVENT] account_id: 3, name: Legal Ska, with <AppointmentForm name:name, date:2019-03-12 15:00:00 +01:00, email:test@fastmail.com, date_from:>
appointment-queries: 0
events-created: 3
new-users: 0
total-users: 0
new-accounts: 0
total-accounts: 0"

    assert_equal(expected, output.string)
  end

  test 'counts the sign-ups' do
    output = StringIO.new
    User.first.update(created_at: '2018-05-16 15:52:39', account: accounts(:one))

    expected = "appointment-queries: 0
events-created: 0
new-users: 1
total-users: 1
new-accounts: 0
total-accounts: 0"

    Metrics.call(file_fixture('metrics_events.log'), output, '2018-05-16')

    assert_equal(expected, output.string)
  end

  test 'counts the number of connected accounts' do
    output = StringIO.new
    Account.first.update!(created_at: '2018-05-16 15:52:39')

    expected = "appointment-queries: 0
events-created: 0
new-users: 0
total-users: 0
new-accounts: 1
total-accounts: 1"

    Metrics.call(file_fixture('metrics_events.log'), output, '2018-05-16')

    assert_equal(expected, output.string)
  end
end
