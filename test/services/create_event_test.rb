require 'test_helper'

class CreateEventTest < ActiveSupport::TestCase
  include ActionMailer::TestHelper

  setup do
    @answers = OpenStruct.new(date: Time.utc(2011, 11, 11), email: 'laura@example.com', name: 'Laura', guests: 5)
    @account = accounts(:one)
    account_settings(:one).update!(account: @account, name: 'Rock law', max_guests: 10)
    @event_duration = CreateEvent::AVERAGE_DURATION
    @time_interval = @account.account_setting.duration.minutes
  end

  test 'calls the provider that creates the event' do
    kaccount = Kloudless::Account.new(@account.remote_id, @account.auth_token)
    kcalendar = Kloudless::Calendar.new(@account.calendar_id, @account.calendar_name)
    event = Kloudless::Event.new("Cita con #{@answers.name}", kaccount, kcalendar, @answers.date, @answers.date + @event_duration, @answers.name, @answers.email)

    Kloudless::Events.expects(:create).with(event)

    CreateEvent.call(@account, @answers)
  end

  test 'rescues errors from kloudless' do
    Kloudless::Events.stubs(:create).raises(Kloudless::Error)

    assert_raises(CreateEvent::CalendarError) do
      CreateEvent.call(@account, @answers)
    end
  end

  test 'calls the mailer to send the confirmation' do
    Kloudless::Events.stubs(:create)
    assert_enqueued_email_with(
      AppointmentMailer,
      :confirmation,
      args: {
        date: @answers.date.iso8601,
        email: @answers.email,
        firm_name: @account.account_setting.name,
        person_name: @answers.name,
        address: @account.account_setting.full_address
      }
    ) do
      CreateEvent.call(@account, @answers)
    end
  end

  test 'calls the mailer to report the account owner that a new appintment was created' do
    Kloudless::Events.stubs(:create)
    expected_email = @account.user.email
    assert_enqueued_email_with(
      AppointmentMailer,
      :created,
      args: { date: @answers.date.iso8601, email: expected_email, person_name: @answers.name, text: @answers.text }
    ) do
      CreateEvent.call(@account, @answers)
    end
  end

  test 'checks that the slot is still free' do
    Kloudless::Events.stubs(:create)
    Occupancy.create!(start_time: @answers.date, end_time: @answers.date + @event_duration, account: @account, guests: 10)

    assert_raises(CreateEvent::TimeNotAvailableError) do
      CreateEvent.call(@account, @answers)
    end
  end

  test 'creates a new occupancy record' do
    Kloudless::Events.stubs(:create)

    CreateEvent.call(@account, @answers)

    created_record = Occupancy.where(account: @account).first
    assert_equal @answers.date, created_record.start_time
    assert_equal @answers.date + @event_duration, created_record.end_time
  end

  test 'updates the occupancy' do
    Kloudless::Events.stubs(:create)
    occupancy = Occupancy.create!(start_time: @answers.date, end_time: @answers.date + @event_duration, account: @account, guests: 5)

    CreateEvent.call(@account, @answers)

    occupancy.reload
    assert_equal 10, occupancy.guests
  end

  test 'creates new occupancy when slots found at the end of the event' do
    Kloudless::Events.stubs(:create)
    occupancy_starts = @answers.date - 20.minutes
    occupancy_ends = occupancy_starts + @event_duration

    first_occupancy = Occupancy.create!(start_time: occupancy_starts, end_time: occupancy_ends, account: @account, guests: 5)

    CreateEvent.call(@account, @answers)

    first_occupancy.reload

    new_occupancy = Occupancy.where("start_time = :start", {start: occupancy_ends}).first
    assert_equal 2, Occupancy.count
    assert_equal 10, first_occupancy.guests
    assert_equal 5, new_occupancy.guests
    assert_equal (@answers.date + CreateEvent::AVERAGE_DURATION), new_occupancy.end_time
  end

  test 'creates new occupancy when slots found at the beginning of the event' do
    Kloudless::Events.stubs(:create)
    first_occupancy = Occupancy.create!(start_time: @answers.date, end_time: @answers.date + @event_duration, account: @account, guests: 5)

    @answers.date = @answers.date - 20.minutes
    CreateEvent.call(@account, @answers)

    first_occupancy.reload

    new_occupancy = Occupancy.where(start_time: @answers.date).first
    assert_equal 2, Occupancy.count
    assert_equal 10, first_occupancy.guests
    assert_equal 5, new_occupancy.guests
    assert_equal first_occupancy.start_time, new_occupancy.end_time
  end
end
