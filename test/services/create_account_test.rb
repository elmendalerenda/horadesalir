require 'test_helper'

class CreateAccountTest < ActiveSupport::TestCase
  test 'creates an account' do
    the_account = CreateAccount.call('legal rock firm')

    assert_equal('legal rock firm', the_account.account_setting.name)
  end

  test 'assigns remote config' do
    the_account = CreateAccount.call('legal rock firm')
    Kloudless::Auth.expects(:verify_token).with('remote_id', 'access_token')

    CreateAccount.add_remote(the_account, 'remote_id', 'access_token')

    the_account.reload
    assert_equal('remote_id', the_account.remote_id)
    assert_equal('access_token', the_account.auth_token)
  end

  test 'handle error verifying token' do
    the_account = CreateAccount.call('legal rock firm')
    Kloudless::Auth.stubs(:verify_token).raises(Kloudless::Error)

    assert_raises(CreateAccount::Error) do
      CreateAccount.add_remote(the_account, 'remote_id', 'access_token')
    end
  end

  test 'initializes a set of available periods' do
    the_account = CreateAccount.call('any_string')

    assert_equal(5, the_account.available_period.size)
    assert_equal(9, the_account.available_period.first.start_hour)
    assert_equal(17, the_account.available_period.first.end_hour)
  end
end
