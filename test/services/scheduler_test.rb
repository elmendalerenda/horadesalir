require 'test_helper'

class SchedulerTest < ActiveSupport::TestCase
  setup do
    @any_account = Kloudless::Account.new('id', 'auth_token')
    @any_calendar = Kloudless::Calendar.new('id', 'name')
    @account = Account.new(
      remote_id: @any_account.id,
      auth_token: @any_account.auth_token,
      calendar_id: @any_calendar.id,
      calendar_name: @any_calendar.name
    )
    @account.account_setting = AccountSetting.new(timezone: 'Europe/Madrid')
    @duration = 60
    @date_from = Time.utc(2017, 3, 22, 0, 0, 0)
    @date_to = Time.utc(2017, 3, 23, 0, 0, 0)
  end

  test '#free_slots splits a large time window in chunks' do
    @account.save!
    AvailablePeriod.create!(start_hour: 12, start_minute: 0, end_hour: 16, end_minute: 0, day_of_week: @date_from.wday, account: @account)
    AvailablePeriod.create!(start_hour: 12, start_minute: 0, end_hour: 16, end_minute: 0, day_of_week: @date_to.wday, account: @account)

    result = Scheduler.query_free_slots(@account, @duration, @date_from, @date_to)

    assert_equal(8, result.count)
    assert_equal('2017-03-22T12:00:00+01:00', result.first.start.iso8601)
    assert_equal('2017-03-22T13:00:00+01:00', result.first.end.iso8601)
    assert_equal('2017-03-23T15:00:00+01:00', result.last.start.iso8601)
    assert_equal('2017-03-23T16:00:00+01:00', result.last.end.iso8601)
  end

  test '#free_slots obtain slots from different time windows' do
    @account.save!
    AvailablePeriod.create!(start_hour: 12, start_minute: 0, end_hour: 19, end_minute: 0, day_of_week: @date_from.wday, account: @account)
    Occupancy.create!(start_time: '2017-03-22T15:00:00Z', end_time: '2017-03-22T17:00:00Z' , account: @account, guests: 1)
    Occupancy.create!(start_time: '2017-03-22T13:00:00Z', end_time: '2017-03-22T14:00:00Z' , account: @account, guests: 1)
    Occupancy.create!(start_time: '2017-03-22T11:00:00Z', end_time: '2017-03-22T12:00:00Z' , account: @account, guests: 1)

    result = Scheduler.query_free_slots(@account, @duration, @date_from, @date_to)

    assert_equal(3, result.count)

    assert_equal('2017-03-22T13:00:00+01:00', result[0].start.iso8601)
    assert_equal('2017-03-22T14:00:00+01:00', result[0].end.iso8601)

    assert_equal('2017-03-22T15:00:00+01:00', result[1].start.iso8601)
    assert_equal('2017-03-22T16:00:00+01:00', result[1].end.iso8601)

    assert_equal('2017-03-22T18:00:00+01:00', result[2].start.iso8601)
    assert_equal('2017-03-22T19:00:00+01:00', result[2].end.iso8601)
  end


  test '#free_slots takes into account the account max_guests' do
    @account.save!
    Occupancy.create!(start_time: '2017-03-22T11:00:00Z', end_time: '2017-03-22T12:00:00Z' , account: @account, guests: 5)
    AvailablePeriod.create!(start_hour: 12, start_minute: 0, end_hour: 16, end_minute: 0, day_of_week: @date_from.wday, account: @account)
    @account.account_setting.update(max_guests: 10)

    result = Scheduler.query_free_slots(@account, @duration, @date_from, @date_to, 4)

    assert_equal(4, result.count)
    assert_equal('2017-03-22T12:00:00+01:00', result.first.start.iso8601)
    assert_equal('2017-03-22T13:00:00+01:00', result.first.end.iso8601)
    assert_equal('2017-03-22T13:00:00+01:00', result[1].start.iso8601)
    assert_equal('2017-03-22T14:00:00+01:00', result[1].end.iso8601)
  end
end
