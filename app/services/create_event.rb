class CreateEvent
  class Error < StandardError; end
  class CalendarError < CreateEvent::Error; end
  class TimeNotAvailableError < CreateEvent::Error; end
  AVERAGE_DURATION = 90.minutes

  class << self

    def call(account, answers)
      log_create(account, answers)

      event = build_event(account, answers)
      validate_event!(event, account, answers.guests)
      Kloudless::Events.create(event)
      save_event(event, answers, account)

      send_confirmation(account, answers)
      send_created(account, answers)
    rescue Kloudless::Error, Kloudless::NetException => e
      Rails.logger.error("Rescued error #{e.message} caused by #{e.cause}")
      raise CreateEvent::CalendarError, "Error Creating the event with #{answers}, #{account}"
    end

    private

    def save_event(event, answers, account)
      overlapping_events = Occupancy.conflicting_desc(event.time_start, event.time_end, account)

      if(overlapping_events.empty?)
        Occupancy.create(start_time: event.time_start, end_time: event.time_end, account: account, guests: answers.guests)
      else
        overlapping_events.each do |e|
          e.update(guests: e.guests + answers.guests)
        end

        if(overlapping_events.first.end_time < event.time_end)
          Occupancy.create(start_time: overlapping_events.first.end_time, end_time: event.time_end, account: account, guests: answers.guests)
        end

        if(overlapping_events.last.start_time > event.time_start)
          Occupancy.create(start_time: event.time_start, end_time: overlapping_events.last.start_time, account: account, guests: answers.guests)
        end
      end
    end

    def validate_event!(event, account, guests)
      min_guests = account.account_setting.max_guests - guests
      return if !Occupancy.any_conflict?(event.time_start, event.time_end, account, min_guests)

      Rails.logger.error("Slot not available for #{event}")
      raise CreateEvent::TimeNotAvailableError, "Slot not available for #{event}"
    end

    def send_confirmation(account, answers)
      AppointmentMailer.with(
        date: answers.date.iso8601,
        email: answers.email,
        firm_name: account.account_setting.name,
        person_name: answers.name,
        address: account.account_setting.full_address
      ).confirmation.deliver_later
    end

    def send_created(account, answers)
      AppointmentMailer.with(
        date: answers.date.iso8601,
        email: account.user.email,
        text: answers.text,
        person_name: answers.name
      ).created.deliver_later
    end

    def build_event(account, answers)
      kaccount = Kloudless::Account.new(account.remote_id, account.auth_token)
      kcalendar = Kloudless::Calendar.new(account.calendar_id, account.calendar_name)
       Kloudless::Event.new("Cita con #{answers.name}", kaccount, kcalendar, answers.date, answers.date + CreateEvent::AVERAGE_DURATION, answers.name, answers.email, answers.text)
    end

    def log_create(account, answers)
      Rails.logger.info("[CREATE EVENT] account_id: #{account.id}, name: #{account.account_setting.name}, with #{answers}")
    end
  end
end
