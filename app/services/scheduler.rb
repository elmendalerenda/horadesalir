class Scheduler
  TimeWindow = Struct.new(:start, :end)

  class << self
    def query_free_slots(account, duration_minutes, date_from, date_to, n_guests=1)
      opening_hours = constraint_with_available_periods(account, date_from, date_to)
      available_slots = remove_busy_slots(opening_hours, account, n_guests)
      duration_slots = chunk_slots(available_slots, duration_minutes, account)

      build_time_windows(duration_slots, duration_minutes)
    end

    private

    def remove_busy_slots(opening_hours, account, n_guests)
      filter_n_guests = account.account_setting.max_guests - n_guests
      availability = []
      opening_hours.each do |t|
        busy = Occupancy.conflicting_asc(t[:start], t[:end], account, filter_n_guests).to_a

        if(busy.empty?)
          availability << {start: t[:start], end: t[:end]}
          next
        end

        availability << {start: t[:start], end: busy.first.start_time }
        busy.each_with_index do |r, i|
          if(r == busy.last)
            availability << {start: r.end_time, end: t[:end] }
          else
            availability << {start: r.end_time, end: busy[i+1].start_time}
          end
        end
      end

      return availability
    end

    def constraint_with_available_periods(account, date_from, date_to)
      time_windows = []
      date_from.to_datetime.upto(date_to.to_datetime) do |day|
        time_window = constraint_time_for_day(day, account)
        time_windows << time_window unless time_window.nil?
      end
      time_windows
    end

    def constraint_time_for_day(day, account)
      period_day_of_week = account.available_period.find { |period| period.day_of_week == day.cwday }
      return nil if period_day_of_week.nil?

      {
        start: account.account_setting.new_time_with_zone(day.year, day.month, day.day, period_day_of_week.start_hour, period_day_of_week.start_minute).utc(),
        end: account.account_setting.new_time_with_zone(day.year, day.month, day.day, period_day_of_week.end_hour, period_day_of_week.end_minute).utc()
      }
    end

    def chunk_slots(available_slots, duration, account)
      duration_slots = []
      available_slots.each do |available_slot|
        start = available_slot[:start]

        while duration_fits_in_slot?(start, available_slot, duration, account)
          duration_slots << account.account_setting.parse_time_in_zone(start.to_s)
          start += duration.minutes
        end
      end
      duration_slots
    end

    def duration_fits_in_slot?(start, slot, duration, account)
      start + duration.minutes <= slot[:end]
    end

    def build_time_windows(dates, duration_minutes)
      duration = duration_minutes.minutes
      dates.map { |d| TimeWindow.new(d, d + duration) }
    end
  end
end
