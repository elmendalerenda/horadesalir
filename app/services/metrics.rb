class Metrics
  class << self
    def call(log_file, output, date)
      count_slot_queries_and_events(log_file, output, date)
      count_users(output, date)
      count_accounts(output, date)
    end

    private

    def count_slot_queries_and_events(log_file, output, date)
      total_queries = 0
      total_events = 0

      File.readlines(log_file).each do |line|
        scanner = StringScanner.new(line)

        next if scanner.scan_until(/#{date}/).nil?

        unless scanner.scan_until(/\[QUERY SLOTS\]/).nil?
          output << scanner.string
          total_queries += 1
        end

        unless scanner.scan_until(/\[CREATE EVENT\]/).nil?
          output << scanner.string
          total_events += 1
        end
      end

      output << "appointment-queries: #{total_queries}\nevents-created: #{total_events}"
    end

    def count_users(output, date)
      day_after_date = (Time.parse(date) + 1.day).strftime('%Y-%m-%d')
      count_new = User.where('created_at >= ? AND created_at <= ?', date, day_after_date).count
      count_all = User.where('created_at <= ?', day_after_date).count

      output << "\nnew-users: #{count_new}"
      output << "\ntotal-users: #{count_all}"
    end

    def count_accounts(output, date)
      day_after_date = (Time.parse(date) + 1.day).strftime('%Y-%m-%d')
      count_new = Account
                  .where.not(remote_id: nil)
                  .where('created_at >= ? AND created_at <= ?', date, day_after_date)
                  .count
      count_all = Account
                  .where.not(remote_id: nil)
                  .where('created_at <= ?', day_after_date)
                  .count

      output << "\nnew-accounts: #{count_new}"
      output << "\ntotal-accounts: #{count_all}"
    end
  end
end
