class Occupancy < ApplicationRecord
  belongs_to :account

  scope :conflicting, ->(start_time, end_time, account) do
    where("((:start BETWEEN start_time AND end_time) OR (:end BETWEEN start_time AND end_time) OR (start_time > :start AND end_time < :end)) AND account_id = :id",
    {start: start_time, end: end_time, id: account.id})
  end

  scope :min_guests, ->(n) { where("guests > :guests", {guests: n}) }

  def self.any_conflict?(start_time, end_time, account, min_guests)
    conflicting(start_time, end_time, account).min_guests(min_guests).exists?
  end

  def self.conflicting_asc(start_time, end_time, account, min_guests)
    conflicting(start_time, end_time, account).min_guests(min_guests).order(start_time: :asc)
  end

  def self.conflicting_desc(start_time, end_time, account)
    conflicting(start_time, end_time, account).order(end_time: :desc)
  end
end
