class AccountSetting < ApplicationRecord
  belongs_to :account

  DEFAULT_DURATION = 60
  MIN_DURATION = 5
  DEFAULT_TIMEZONE = 'Europe/Madrid'.freeze

  validates :name, presence: true
  validates :address, length: { maximum: 150 }
  validates :duration, presence: true, numericality: { only_integer: true, greater_than_or_equal_to: MIN_DURATION }
  validates :timezone, presence: true
  validate :valid_timezone?

  before_save :name_to_slug

  def new_time_with_zone(year, month, day, hour, min, sec = 0)
    Time.new(year, month, day, hour, min, sec, timezone_object.formatted_offset)
  end

  def parse_time_in_zone(string)
    Time.parse(string).in_time_zone(timezone_object)
  end

  def update_form_settings(duration, address)
    update(duration: duration, address: address)
  end

  def full_address
    "#{name}, #{address}"
  end

  private

  def name_to_slug
    return unless name_changed?

    suffix = AccountSetting.exists?(name: name) ? "-#{account_id}" : ''

    self.slug = name.parameterize.truncate(80, omission: '', separator: ' ') + suffix
  end

  def timezone_object
    @timezone_object ||= Time.find_zone(timezone)
  end

  def valid_timezone?
    return unless timezone.present?

    Time.find_zone!(timezone)
  rescue ArgumentError
    errors.add(:timezone, :invalid)
  end
end
