document.addEventListener("turbolinks:load", function() {
  var url = new URL(window.location.href);
  var c = url.searchParams.get("welcome");
  if(c){
    $('#welcome-modal').modal();
  }
});

function enableInputsOnChecked(){
  $("input[type='checkbox']").on("change",
    function(event){
      var id = event.target.value
      var start = $("#periods_start_"+id)
      var end = $("#periods_end_"+id)
      var disabled = event.target.checked == false

      start.prop('disabled', disabled);
      end.prop('disabled', disabled);
      if(!disabled)
      {
        start.prop('value', '09:00');
        end.prop('value', '17:00');
      }
      else
      {
        end.prop('value', '');
        start.prop('value', '');
      }
    }
  )
}
$( document ).ready( enableInputsOnChecked );

(() => {
  stimulus.register("account-profiles", class extends Stimulus.Controller {
    static get targets() {
      return [ ]
    }

    copy(event) {
      const el = document.createElement('textarea');
      el.value = event.target.dataset['url'];
      document.body.appendChild(el);
      el.select();
      document.execCommand('copy');
      document.body.removeChild(el);

      const $message = $('#clipboard-alert');
      $message.collapse('show');

      setTimeout(function() {
        $message.collapse('hide');
      }, 2000);
    }
  })
})()
