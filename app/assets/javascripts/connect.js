(() => {
  stimulus.register("connect", class extends Stimulus.Controller {
    static get targets() {
      return [ "id", "accessToken" ]
    }

    auth(event) {
      event.preventDefault();

      let config = {
        'client_id': this.data.get('appid'),
        'scope': this.data.get('scope')
      };

      var that = this;
      let callback = function (result) {
        if (result.error) {
          console.error('An error occurred:', result.error);
          that.displayError();
          return;
        }

        that.idTarget.value = result.account.id;
        that.accessTokenTarget.value = result.access_token;

        if(!result.account.id) {
          that.displayError();
          return;
        }

        that.element.submit();
        event.target.disabled = true;
      };

      let auth = Kloudless.authenticator(null, config, callback);
      auth.launch();
    }

    displayError() {
      document.querySelector('#auth-error').classList.remove('d-none')
    }

  })
})()
