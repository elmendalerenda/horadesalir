class AccountProfilesController < ApplicationController
  before_action :authenticate_user!
  before_action :account_present!

  def index
    periods = AvailablePeriodForm.from_models(AvailablePeriod.find_by_account(account))

    periods_table_data(periods)
    links_data
    hide_alerts
  end

  def update
    periods = UpdateAvailablePeriods.call(account, periods_params)
    update_settings

    periods_table_data(periods)
    links_data
    show_alerts(periods)

    respond_to do |format|
      format.html { render :index }
      format.js
    end
  end

  private

  def show_alerts(periods)
    @success = periods.all? { |p| p.errors.empty? } && @account_settings.valid?
    @error = !@success
  end

  def hide_alerts
    @success = nil
    @error = nil
  end

  def update_settings
    account.account_setting.update_form_settings(duration_params[:minutes].to_i, address_params[:address])
  end

  def duration_params
    params.require(:duration).permit(:minutes)
  end

  def address_params
    params.permit(:address)
  end

  def periods_params
    params.require(:periods)
  end

  def links_data
    @form_url = appointment_url(account.account_setting.slug, host: t('host'))
  end

  def periods_table_data(periods)
    @account_id = account.id
    @periods = periods + missing_periods(periods)
    @account_settings = account.account_setting
  end

  def missing_periods(periods)
    days_saved = periods.map(&:day_of_week)
    days_missing = (1..7).to_a - days_saved

    days_missing.map do |d|
      AvailablePeriodForm.new.tap { |ap| ap.day_of_week = d }
    end
  end

  def account
    current_user.account
  end

  def account_present!
    redirect_to setup_account_index_path unless account.connected?
  end
end
