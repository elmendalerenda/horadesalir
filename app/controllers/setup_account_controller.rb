class SetupAccountController < ApplicationController
  before_action :authenticate_user!
  layout 'unauthenticated'

  def index
    @app_id = Kloudless.configuration.app_id
    @scope = Kloudless.configuration.scope
  end

  def connect
    id, access_token = params_account
    updated_account = CreateAccount.add_remote(account, id, access_token)

    @calendars = Kloudless::Calendars.find_all(Kloudless::Account.new(updated_account.remote_id, updated_account.auth_token))
  rescue Kloudless::Error, CreateAccount::Error => e
    @error_message = e.message
  end

  def calendar
    calendar_id, calendar_name, calendar_timezone = params_calendar
    CreateAccount.add_calendar(account, calendar_id, calendar_name, calendar_timezone)

    redirect_to account_profiles_path(show_welcome_modal)
  end

  private

  def show_welcome_modal
    { welcome: 'true' }
  end

  def account
    current_user.account
  end

  def params_calendar
    params.require(:account).require(%i[calendar_id calendar_name calendar_timezone])
  end

  def params_account
    params.require(:account).require(%i[id access_token])
  end
end
