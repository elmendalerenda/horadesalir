class ApplicationMailer < ActionMailer::Base
  default from: 'es@horadesalir.org'
  layout 'mailer'
end
