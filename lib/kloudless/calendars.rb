module Kloudless
  class Calendars
    def self.find_all(account)
      return [Kloudless::Calendar.dev] if account == Account.dev

      Rails.logger.info("Requesting calendars for #{account}")
      uri = URI("http://api.kloudless.com:443/v1/accounts/#{account.id}/cal/calendars")
      req = Net::HTTP::Get.new(uri)
      req['Content-Type'] = 'application/json'
      req['Authorization'] = "Bearer #{account.auth_token}"

      http = Net::HTTP.new(uri.hostname, uri.port)
      http.use_ssl = true
      begin
        res = http.start { |conn| conn.request(req) }
      rescue StandardError => e
        Rails.logger.error("Network Error getting the calendars for #{account}:  #{e.message}")
        raise Kloudless::NetException
      end

      body = JSON.parse(res.body)

      if body['error_code'].present?
        Rails.logger.error("Remote server error received: #{body}")
        raise Kloudless::Error
      end

      body['objects'].map { |c| Kloudless::Calendar.new(c['id'], c['name'], c['timezone']) }
    end
  end
end
