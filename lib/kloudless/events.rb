module Kloudless
  class Events
    class << self
      # https://developers.kloudless.com/docs/v1/calendar#events-create-an-event
      def create(event)
        Rails.logger.info("Creating event for #{event}")

        uri = URI("https://api.kloudless.com/v1/accounts/#{event.account.id}/cal/calendars/#{event.calendar.id}/events")
        req = Net::HTTP::Post.new(uri, 'Content-Type' => 'application/json', 'Authorization' => "Bearer #{event.account.auth_token}")

        req.body = body(event)

        http = Net::HTTP.new(uri.hostname, uri.port)
        http.use_ssl = true

        begin
          res = http.start { |http_req| http_req.request(req) }
        rescue StandardError => e
          Rails.logger.error("Network Error getting creating the event #{e.message}")
          raise Kloudless::NetException
        end

        body = JSON.parse(res.body)
        if body['error_code'].present?
          Rails.logger.error("Remote server error received: #{body}")
          raise Kloudless::Error
        end
      end

      private

      def body(event)
        { name: event.name,
          start: event.time_start.iso8601,
          end: event.time_end.iso8601,
          description: event.description,
          organizer: { name: event.person_name, email: event.person_email } }.to_json
      end
    end
  end
end
