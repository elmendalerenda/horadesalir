task metrics: :environment do
  file_name = ENV['RAILS_ENV'].nil? ? 'development.log' : 'puma_access.log'
  yesterday = (Time.now - 1.day).strftime('%Y-%m-%d')

  File.open("./log/metrics#{yesterday}.log", 'w') do |output|
    Metrics.call("./log/#{file_name}", output, yesterday)
  end
end
